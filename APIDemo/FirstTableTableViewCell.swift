//
//  FirstTableTableViewCell.swift
//  APIDemo
//
//  Created by Sukhwinder Rana on 2019-07-01.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit

class FirstTableTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var teamAImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func fifaData() {
//        let fifa = db.collection("Fifa")
//        
//        fifa.document("quarter").setData([
//            "1": [
//                "Id" : "1",
//                "date": "27-June",
//                "Time": " 21:00",
//                "Location": "Stade Oceane",
//                "Latitude": 49.49,
//                "longitude": 0.10,
//                "city": "Le Havre",
//                "TeamA": "norway",
//                "TeamB": "england"
//            ],
//            "2": [
//                "Id" : "2",
//                "date": "28-June",
//                "Time": " 21:00",
//                "Location": "parc Des Princes",
//                "Latitude": 48.85,
//                "longitude": 2.35,
//                "city": "Paris",
//                "TeamA": "france",
//                "TeamB": "usa"
//            ],
//            "3": [
//                "Id" : "3",
//                "date": "29-June",
//                "Time": " 15:00",
//                "Location": "Stade Du Hainaut",
//                "Latitude": 50.35,
//                "longitude": 3.51,
//                "city": "Valenciennes",
//                "TeamA": "italy",
//                "TeamB": "netherlands"
//            ],
//            "4": [
//                "Id" : "4",
//                "date": "29-June",
//                "Time": " 18:30",
//                "Location": "Roazhon Park",
//                "Latitude": 48.11,
//                "longitude": 1.67,
//                "city": "Rennes",
//                "TeamA": "germany",
//                "TeamB": "swedan"
//            ]
//            ])
//        fifa.document("SemiFinal").setData([
//            "5": [
//                "Id" : "5",
//                "date": "2-July",
//                "Time": " 21:00",
//                "Location": "Stade De Lyon",
//                "Latitude": 45.76,
//                "longitude": 4.83,
//                "city": "Lyon",
//                "TeamA": "england",
//                "TeamB": "usa"
//            ],
//            "6": [
//                "Id" : "6",
//                "date": "3-July",
//                "Time": " 21:00",
//                "Location": "Stade De Lyon",
//                "Latitude": 45.76,
//                "longitude": 4.83,
//                "city": "Lyon",
//                "TeamA": "netherlands",
//                "TeamB": "swedan"
//            ]
//            ])
//        fifa.document("ThirdPlace").setData([
//            "7": [
//                "Id" : "6",
//                "date": "6-July",
//                "Time": " 21:00",
//                "Location": "Stade De Nice",
//                "Latitude": 43.71,
//                "longitude": 7.26,
//                "city": "Nice",
//                "TeamA": "un",
//                "TeamB": "un"
//            ]
//            ])
//        fifa.document("Final").setData([
//            "8": [
//                "Id" : "8",
//                "date": "7-July",
//                "Time": " 21:00",
//                "Location": "Stade De Lyon",
//                "Latitude": 45.76,
//                "longitude": 4.83,
//                "city": "Lyon",
//                "TeamA": "un",
//                "TeamB": "un"
//            ]
//            ])
//    }
//    


}
