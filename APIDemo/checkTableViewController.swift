//
//  checkTableViewController.swift
//  APIDemo
//
//  Created by Sukhwinder Rana on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import WatchConnectivity

class checkTableViewController: UITableViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    func sessionDidBecomeInactive(_ session: WCSession) {
    }
    func sessionDidDeactivate(_ session: WCSession) {
    }
    
    var game:[Game] = []
    var teamA:[String] = []
    var teamB:[String] = []
    var dates:[String] = []
    var subscribers:[Int] = []
    var SubscriberData:[Game] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        navigateBarItem()
        
    
        
   
        if(WCSession.isSupported()){
            print("Phone Second Page support watch")
            let session = WCSession.default
            session.delegate = self
            session.activate()
            
        }
        else{
            print("phone second page does support watch")
        }
        
        print("------------===== Check game array on second page =====------------")
        for (_, data) in self.game.enumerated(){
            print("======= game Array data check second page =========")
            print("game type is :\(data.gameType!)")
            print("============================")
        }
        for (_, data) in game.enumerated(){
            teamA.append(data.TeamA)
            teamB.append(data.TeamB)
            dates.append(data.date)
          //  print("TeamA TeamB and Date array is being filled ")
            
        }
        
       
          
                if(self.subscribers.isEmpty == false){
                    for (_, d)  in self.subscribers.enumerated(){
                        for (j, _) in self.teamA.enumerated(){
                     
                          if (d == j){
                            self.SubscriberData.append(game[d])
                           
                        }
                        
                    }
                }
        
            }
        if(WCSession.default.isReachable){
            print("watch has been found for second page")
            let m = [
                "list1":self.subscribers,
            ]
//
//            var d = WCSession.default.receivedApplicationContext
//            print("received:\(d)")
            WCSession.default.sendMessage(m, replyHandler:nil)
    
            } else{
            print("watch not found on second page")
        }
         print("Subscriber array data is: \(self.SubscriberData)")
        if(!subscribers.isEmpty){
            print("subscriber arry index is:\(subscribers)")
        }
    }
    
    private func   navigateBarItem(){
        let titleimage = UIImageView(image: UIImage(named: "exit"))
        titleimage.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        titleimage.contentMode = .scaleAspectFit
        navigationItem.titleView = titleimage
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "home"), for: .normal)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
//        let barButtonItem = UIBarButtonItem(title: "Left Item", style: .plain, target: self, action: nil)
//            barButtonItem.tintColor = UIColor.red
          //  return barButtonItem
      //  }()
        
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.teamA.count

       
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "checkCell", for: indexPath) as! checkTableViewCell
        if(self.subscribers.isEmpty == false){
            for (_, d )  in self.subscribers.enumerated(){
            if (indexPath.row == d){
                cell.isUserInteractionEnabled = false
                cell.subscribeLabel.text = "Subscribed"
                cell.subscribeLabel.textColor = UIColor.green
                cell.selectionStyle =  UITableViewCellSelectionStyle.blue
            }
            
        }
        }
        
        cell.checkImageA.image = UIImage(named: teamA[indexPath.row])
        cell.checkImageB.image = UIImage(named: teamB[indexPath.row])
        cell.chcekLabel.text = dates[indexPath.row]
        cell.subscribeLabel.text = "Subscribe me!!"
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "gameDetail", sender: nil)
           // for (_, d )  in self.subscribers.enumerated(){
              //  if (indexPath.row == d){
                    let message = "You Already have subscribed this Match"
                    let infoAlert = UIAlertController(title: "Already Subscribed", message: message, preferredStyle: .alert)
                    infoAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
                    self.present(infoAlert, animated: true, completion: nil)
           //     }
                
            }
 //   }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let i = self.tableView.indexPathForSelectedRow?.row
      
            print("Selected row on second Page is: \(i!)")
            let n1 = segue.destination as! GameDetailsViewController
            n1.game = self.game
            n1.selectedRow = i
            n1.subscriber = self.subscribers
            teamA.removeAll()
            teamB.removeAll()
            game.removeAll()
            self.subscribers.removeAll()
        
    }

}

