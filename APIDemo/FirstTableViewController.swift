//
//  FirstTableViewController.swift
//  APIDemo
//
//  Created by Sukhwinder Rana on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import FirebaseFirestore
import CoreLocation
import MapKit
import WatchConnectivity

class FirstTableViewController: UITableViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    func sessionDidBecomeInactive(_ session: WCSession) {
    }
    func sessionDidDeactivate(_ session: WCSession) {
    }
    var manager:CLLocationManager!
    var username = ""
    var db:Firestore!
    var game1:[Game] = []
    var pics:[String] = ["list", "mapicon"]
    var Rowdata:[String] = ["List Of All Games", "Stadium Location"]
   // var game2:[Int:Dictionary<String, Any>]!
   //    var game3:[Int:JSON] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        if(WCSession.isSupported()){
            print("phone support watch connectivity")
            let session = WCSession.default
            session.delegate = self
            session.activate()
            
        }
        else{
            print("phone does not support watch connectivity")
        }
        db = Firestore.firestore()
        manager = CLLocationManager()
        //manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
       let URL = "https://fifa-assignment.firebaseio.com/Fifa.json"
                Alamofire.request(URL).responseJSON {
        
                    response in
      
                    let apiData = response.result.value
                    if (apiData == nil) {
                        print("Error when getting API data")
                        return
                    }
                    let jsonResponse = JSON(apiData!)
                    
                    
                    print("============json response==============")
                    print(jsonResponse)
                    print("============================")
                    var d = jsonResponse["quarter"]["3"].dictionary
                    
                    
                                       // self.game2[0] = jsonResponse["quarter"]["3"].dictionary
//                                        self.game2[1] = jsonResponse["quarter"]["4"].dictionary
//                                        self.game2[2] = jsonResponse["semiFinal"]["5"].dictionary
//                                        self.game2[3] = jsonResponse["semiFinal"]["6"].dictionary
//                                        self.game2[4] = jsonResponse["third"]["7"].dictionary
//                                        self.game2[5] = jsonResponse["final"]["8"].dictionary
//
////
                    self.game1.append(Game.init(gameType: "quarter", city: d!["city"]!.string!, date: d!["date"]!.string!, location: d!["Location"]!.string!, teamA: d!["TeamA"]!.string!, teamB: d!["TeamB"]!.string!, latlng: CLLocationCoordinate2D(latitude: d!["Latitude"]!.double!, longitude: d!["longitude"]!.double!),time: d!["Time"]!.string!))
                    
                    d = jsonResponse["quarter"]["4"].dictionary
                    
                    self.game1.append(Game.init(gameType: "quarter", city: d!["city"]!.string!, date: d!["date"]!.string!, location: d!["Location"]!.string!, teamA: d!["TeamA"]!.string!, teamB: d!["TeamB"]!.string!, latlng: CLLocationCoordinate2D(latitude: d!["Latitude"]!.double!, longitude: d!["longitude"]!.double!),time: d!["Time"]!.string!))
                    
                    d = jsonResponse["semiFinal"]["5"].dictionary
                    
                     self.game1.append(Game.init(gameType: "semiFinal", city: d!["city"]!.string!, date: d!["date"]!.string!, location: d!["Location"]!.string!, teamA: d!["TeamA"]!.string!, teamB: d!["TeamB"]!.string!, latlng: CLLocationCoordinate2D(latitude: d!["Latitude"]!.double!, longitude: d!["longitude"]!.double!),time: d!["Time"]!.string!))
                    
                    d = jsonResponse["semiFinal"]["6"].dictionary
                    
                    self.game1.append(Game.init(gameType: "semiFinal", city: d!["city"]!.string!, date: d!["date"]!.string!, location: d!["Location"]!.string!, teamA: d!["TeamA"]!.string!, teamB: d!["TeamB"]!.string!, latlng: CLLocationCoordinate2D(latitude: d!["Latitude"]!.double!, longitude: d!["longitude"]!.double!),time: d!["Time"]!.string!))
                    
                    d = jsonResponse["third"]["7"].dictionary
                    
                    self.game1.append(Game.init(gameType: "Third", city: d!["city"]!.string!, date: d!["date"]!.string!, location: d!["Location"]!.string!, teamA: d!["TeamA"]!.string!, teamB: d!["TeamB"]!.string!, latlng: CLLocationCoordinate2D(latitude: d!["Latitude"]!.double!, longitude: d!["longitude"]!.double!),time: d!["Time"]!.string!))
                    
                    d = jsonResponse["final"]["8"].dictionary
                    
                    self.game1.append(Game.init(gameType: "final", city: d!["city"]!.string!, date: d!["date"]!.string!, location: d!["Location"]!.string!, teamA: d!["TeamA"]!.string!, teamB: d!["TeamB"]!.string!, latlng: CLLocationCoordinate2D(latitude: d!["Latitude"]!.double!, longitude: d!["longitude"]!.double!),time: d!["Time"]!.string!))
                    
                    
                    print("Check game array is empty or not: \(self.game1.isEmpty)")
                    
                    
                    
                    for (_, data) in self.game1.enumerated(){
                        print("======= game1 data check =========")
                        print("city is :\(data.city!)")
                        print("============================")
                    }
                    
                   if(WCSession.default.isReachable){
                        print("Available watch has been connected")
                      if(self.game1.isEmpty == false){
                        let mobileToWatch = [
                            "list": jsonResponse
                            
                        ]
                            print("data has been sent to Watch")
                            print("===== check data which send to watch using list =====")
                            print(mobileToWatch["list"]!)
                            print("==============================")
                        
//                        var d = WCSession.default.receivedApplicationContext
//                        print("received:\(d)")
                        WCSession.default.sendMessage(mobileToWatch, replyHandler:nil)
                       }
                    } else{
                        print("watch is out of reach")
                    }
   
    }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pics.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myRow", for: indexPath) as! FirstTableTableViewCell
         cell.teamAImage.image = UIImage(named: self.pics[indexPath.row])
        cell.dateLabel.text = self.Rowdata[indexPath.row]
        return cell
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Row \(indexPath.row) Selecteded in map/list")
        if(indexPath.row == 0){
        performSegue(withIdentifier: "check", sender: nil)
        }
        if(indexPath.row == 1){
         performSegue(withIdentifier: "map", sender: nil)
        }
        
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let i = self.tableView.indexPathForSelectedRow?.row
        print("Selected row is: \(i!) for segue perform in map/list")
        if(i == 0){
        let n1 = segue.destination as! checkTableViewController
        n1.game = self.game1
        }
        else if(i == 1){
            let n1 = segue.destination as! mapview
            n1.game = self.game1
        }
    }
}
