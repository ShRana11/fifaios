//
//  mapview.swift
//  APIDemo
//
//  Created by Sukhwinder Rana on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit
import CoreLocation

class mapview: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate{
 var annotation:MKAnnotation!
    var annotations = [MKPointAnnotation]()
    var game:[Game] = []
    var latlong:[CLLocationCoordinate2D] = []
    var pinName:[String] = []
    
    
    @IBOutlet weak var map: MKMapView!
 
    @IBAction func homeButton(_ sender: Any) {
         performSegue(withIdentifier: "mapToHome", sender: nil)
    }
    
    @IBAction func zoomInButton(_ sender: Any) {
           print("zoom in!")
        var r = map.region
        r.span.latitudeDelta = r.span.latitudeDelta / 2
        r.span.longitudeDelta = r.span.longitudeDelta / 2
        self.map.setRegion(r, animated: true)
    }
    @IBAction func zoomOutButton(_ sender: Any) {
        print("zoom out!")
        
        var r = map.region
        r.span.latitudeDelta = r.span.latitudeDelta * 2
        r.span.longitudeDelta = r.span.longitudeDelta * 2
        self.map.setRegion(r, animated: true)
    }
     override func viewDidLoad() {
        super.viewDidLoad()
        print("loaded the map screen")
        self.map.delegate = self
        for (i,d) in game.enumerated(){
           var x = d.Latlng.latitude
            var y = d.Latlng.longitude
            self.latlong.append(CLLocationCoordinate2D(latitude: x, longitude: y))
            self.pinName.append(d.city!)
        }
        var annotations = [MKPointAnnotation]()
        for (index, eachLocation) in latlong.enumerated() {
            let pinImageName = self.pinName[index]
            let annotation = MKPointAnnotation()
            annotation.coordinate = eachLocation
            annotation.title = "\(pinImageName)"
            self.annotations.append(annotation)
        }
        for (i,d) in self.annotations.enumerated(){
            var an = d
         map.addAnnotation(an)
        }
        
        
        let x = CLLocationCoordinate2DMake(50.50, 2.01)
        let y = MKCoordinateSpanMake(12, 12)
        let z = MKCoordinateRegionMake(x, y)
        self.map.setRegion(z, animated: true)
}
}
