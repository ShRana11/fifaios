//
//  GameDetailsViewController.swift
//  APIDemo
//
//  Created by Sukhwinder Rana on 2019-07-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit

class GameDetailsViewController: UIViewController {
    @IBOutlet weak var teamB: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var stadiumLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var teamA: UILabel!
    var selectedRow:Int!
    var game:[Game] = []
    var subscriber:[Int] = []
    var mainSB : UIStoryboard! = nil
    var StyleCVC : checkTableViewController!
 
    @IBAction func HomeButton(_ sender: Any) {
        let vcontrol = storyboard?.instantiateViewController(withIdentifier: "main")
        self.navigationController?.pushViewController(vcontrol!, animated: true)
        
    }
    @IBAction func subscribe(_ sender: Any) {
            self.subscriber.append(self.selectedRow)
      //  print("subscriberrs on third page is: \(self.subscriber)")
      //  print("sub on third page is: \(StyleCVC.subscribers)")
        StyleCVC.subscribers.append(self.selectedRow)
//        var stat = false
//        for (index , data) in subscriber.enumerated(){
//            if ( selectedRow == data ){
//                stat = true
//                print("old row selected ")
//            }
//        }
//        if(stat == false){
    //    StyleCVC.subscribers.append(selectedRow)
        print("subscriberrs on third page is: \(StyleCVC.subscribers)")
        self.present(StyleCVC, animated: true, completion: nil)

       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        let addButton = UIBarButtonItem(title: "Logout", style: .done , target: self, action: #selector(logoutButton))
//        self.navigationItem.rightBarButtonItem = addButton
 
        self.mainSB = UIStoryboard(name: "Main", bundle: nil)
        self.StyleCVC = mainSB.instantiateViewController(withIdentifier: "ListScene") as! checkTableViewController
        StyleCVC.game = self.game
        StyleCVC.subscribers = self.subscriber
         navigationController?.shouldPerformSegue(withIdentifier: "back", sender: nil)
       // print(game[self.selectedRow].date)
        self.teamA.text = game[self.selectedRow].TeamA
        self.teamB.text = game[self.selectedRow].TeamB
        self.timeLabel.text = game[self.selectedRow].Time
        self.dateLabel.text = game[self.selectedRow].date
        self.cityLabel.text = game[self.selectedRow].city
        self.stadiumLabel.text = game[self.selectedRow].Location
        

        // Do any additional setup after loading the view.
    }
//    @objc func logoutButton() {
//      
//        
//    }

}
