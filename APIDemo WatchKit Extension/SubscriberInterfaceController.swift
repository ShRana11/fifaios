//
//  SubscriberInterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Sukhwinder Rana on 2019-07-04.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON


class SubscriberInterfaceController: WKInterfaceController,CLLocationManagerDelegate {
    var jsonResponse:[Game] = []
    var locationManager: CLLocationManager = CLLocationManager()
    var mapLocation: CLLocationCoordinate2D?
    var latlong:[CLLocationCoordinate2D] = []
    var pinName:[String] = []
     
    @IBAction func changeZoom(_ value: Float) {
        let degrees:CLLocationDegrees = CLLocationDegrees(value * 10)
        
        let span = MKCoordinateSpanMake(degrees, degrees)
        let region = MKCoordinateRegionMake(mapLocation!, span)
        
        mapView.setRegion(region)
    }
    @IBOutlet var subscriberTable: WKInterfaceTable!
   
    @IBOutlet var mapView: WKInterfaceMap!
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
   
        
        print("heloo")
        print("=====second page json")
        //print(self.jsonResponse[0].city!)
        print(self.jsonResponse)
        print("=========")
        
        self.subscriberTable.setNumberOfRows(self.jsonResponse.count, withRowType: "myRow")
        
        for (index , name) in jsonResponse.enumerated(){
            let row = self.subscriberTable.rowController(at: index) as! SubscriberCell
            row.imageTeamA.setImage(UIImage(named: name.TeamA))
            
        }
        for (index , name) in jsonResponse.enumerated(){
            let row = self.subscriberTable.rowController(at: index) as! SubscriberCell
            row.imageTeamB.setImage(UIImage(named: name.TeamB))
        }
        for (index , name) in jsonResponse.enumerated(){
            let row = self.subscriberTable.rowController(at: index) as! SubscriberCell
            row.dateLabel.setText(name.date)
        }
        for (i,d) in jsonResponse.enumerated(){
            var x = d.Latlng.latitude
            var y = d.Latlng.longitude
            self.latlong.append(CLLocationCoordinate2D(latitude: x, longitude: y))
            self.pinName.append(d.city!)
        }
        let lat = 50.50
        let long = 2.02
        
        
        self.mapLocation = CLLocationCoordinate2DMake(lat, long)
        latlong.append(self.mapLocation!)
       // self.latlong.append(CLLocationCoordinate2D(latitude: lat + 0.1, longitude: long + 0.1))
        
        let span = MKCoordinateSpanMake(15, 15)
        
        let region = MKCoordinateRegionMake(self.mapLocation!, span)
        self.mapView.setRegion(region)

        for (index , name ) in latlong.enumerated(){
//        mapView.setRegion(MKCoordinateRegion(center: name, span: MKCoordinateSpan(latitudeDelta: CLLocationDegrees(1), longitudeDelta: CLLocationDegrees(1))))
        mapView.addAnnotation(name, with: WKInterfaceMapPinColor.green)
        }
      
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        print("awake")
        self.jsonResponse = context as! Any as! [Game]
        
 //      locationManager.requestWhenInUseAuthorization()
 //      locationManager.desiredAccuracy = kCLLocationAccuracyBest
 //      locationManager.delegate = self
//        locationManager.requestLocation()
    }
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        self.pushController(withName: "thirdPage", context: jsonResponse[rowIndex])
    }

}
