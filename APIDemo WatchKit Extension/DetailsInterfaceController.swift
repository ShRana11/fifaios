//
//  DetailsInterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Sukhwinder Rana on 2019-07-04.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation


class DetailsInterfaceController: WKInterfaceController {
     var jsonResponse:Game!

    @IBOutlet var cityLabel: WKInterfaceLabel!
    @IBOutlet var dateTimeLabel: WKInterfaceLabel!
    @IBOutlet var locationLabel: WKInterfaceLabel!
    @IBOutlet var teamLabel: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
          self.jsonResponse = context as! Any as! Game
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        self.teamLabel.setText("\(jsonResponse.TeamA!) VS \(jsonResponse.TeamB!)")
       self.locationLabel.setText("\(jsonResponse.Location!)")
        self.dateTimeLabel.setText("\(jsonResponse.date!) at \(jsonResponse.Time!)")
        self.cityLabel.setText("\(jsonResponse.city!)")
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
