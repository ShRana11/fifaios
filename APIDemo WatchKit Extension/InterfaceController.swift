//
//  InterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON
import  WatchConnectivity

class InterfaceController: WKInterfaceController,WCSessionDelegate {
    var sub:[Int] = []
    var json:JSON!
    var game1:[Game] = []
    var subscribers:[Game] = []
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?){
    }
 
    @IBOutlet var watchOutputLabel: WKInterfaceLabel!
   
    @IBAction func getDataPressed() {
        print("Watch button pressed")
        if(self.subscribers.isEmpty == false){
        pushController(withName: "secondPage", context: self.subscribers)
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("got message")
        var data = message["list"]
        var data1 = message["list1"]
        self.sub = message["list1"] as! [Int]
        if(data != nil){
        print("data is \(data!)")
        }
        if(data1 != nil){
         print("data1 is \(data1!)")
        }
        
        if (game1.isEmpty == false){
            for (_, d)  in self.sub.enumerated(){
                for (j, _) in self.game1.enumerated(){
                    
                    if (d == j){
                        self.subscribers.append(self.game1[d])
                        print("subscribers are: \(subscribers)")
                    }
                    
                }
            }
        }
       
        //        var ack = [
        //            "result":"reached"
        //        ]
        //        WCSession.default.sendMessage(ack, replyHandler: nil, errorHandler: nil)
    }
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
         watchOutputLabel.setText("Fetching Data...")
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if (WCSession.isSupported()){
            print("Watch supported")
            let session = WCSession.default
            session.delegate = self
            session.activate()
            
        }
        else{
            
            print("watch not supported")
        }
        let URL = "https://fifa-assignment.firebaseio.com/Fifa.json"
        
        Alamofire.request(URL).responseJSON {
            
            response in
            let apiData = response.result.value
            if (apiData == nil) {
                print("Error when getting API data")
                return
            }
            // print(apiData)
            let jsonResponse = JSON(apiData)
            print("Json Response for all Users")
            print("========== All Users ==========")
            print(jsonResponse)
            print("=====================")
            self.json = jsonResponse
            let origin = jsonResponse["origin"]
            let host = jsonResponse["headers"]["Host"]
            
            var d = jsonResponse["quarter"]["3"].dictionary

            self.game1.append(Game.init(gameType: "quarter", city: d!["city"]!.string!, date: d!["date"]!.string!, location: d!["Location"]!.string!, teamA: d!["TeamA"]!.string!, teamB: d!["TeamB"]!.string!, latlng: CLLocationCoordinate2D(latitude: d!["Latitude"]!.double!, longitude: d!["longitude"]!.double!),time: d!["Time"]!.string!))
            
            d = jsonResponse["quarter"]["4"].dictionary
            
            self.game1.append(Game.init(gameType: "quarter", city: d!["city"]!.string!, date: d!["date"]!.string!, location: d!["Location"]!.string!, teamA: d!["TeamA"]!.string!, teamB: d!["TeamB"]!.string!, latlng: CLLocationCoordinate2D(latitude: d!["Latitude"]!.double!, longitude: d!["longitude"]!.double!),time: d!["Time"]!.string!))
            
            d = jsonResponse["semiFinal"]["5"].dictionary
            
            self.game1.append(Game.init(gameType: "semiFinal", city: d!["city"]!.string!, date: d!["date"]!.string!, location: d!["Location"]!.string!, teamA: d!["TeamA"]!.string!, teamB: d!["TeamB"]!.string!, latlng: CLLocationCoordinate2D(latitude: d!["Latitude"]!.double!, longitude: d!["longitude"]!.double!),time: d!["Time"]!.string!))
            
            d = jsonResponse["semiFinal"]["6"].dictionary
            
            self.game1.append(Game.init(gameType: "semiFinal", city: d!["city"]!.string!, date: d!["date"]!.string!, location: d!["Location"]!.string!, teamA: d!["TeamA"]!.string!, teamB: d!["TeamB"]!.string!, latlng: CLLocationCoordinate2D(latitude: d!["Latitude"]!.double!, longitude: d!["longitude"]!.double!),time: d!["Time"]!.string!))
            
            d = jsonResponse["third"]["7"].dictionary
            
            self.game1.append(Game.init(gameType: "Third", city: d!["city"]!.string!, date: d!["date"]!.string!, location: d!["Location"]!.string!, teamA: d!["TeamA"]!.string!, teamB: d!["TeamB"]!.string!, latlng: CLLocationCoordinate2D(latitude: d!["Latitude"]!.double!, longitude: d!["longitude"]!.double!),time: d!["Time"]!.string!))
            
            d = jsonResponse["final"]["8"].dictionary
            
            self.game1.append(Game.init(gameType: "final", city: d!["city"]!.string!, date: d!["date"]!.string!, location: d!["Location"]!.string!, teamA: d!["TeamA"]!.string!, teamB: d!["TeamB"]!.string!, latlng: CLLocationCoordinate2D(latitude: d!["Latitude"]!.double!, longitude: d!["longitude"]!.double!),time: d!["Time"]!.string!))
            
            
            print("Check game array is empty or not: \(self.game1.isEmpty)")
            
            
            
            for (_, data) in self.game1.enumerated(){
                print("======= game1 data check =========")
                print("city is :\(data.city!)")
                print("============================")
            }
            
             self.watchOutputLabel.setText("Fetching Completed")
            self.watchOutputLabel.setTextColor(UIColor.green)
            // 3. Show the data in the user interface
            //  self.watchOutputLabel.setText("\(origin)")
        }
    }
    
   
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        self.watchOutputLabel.setText("Fetching...")
        self.watchOutputLabel.setTextColor(UIColor.red)
        self.subscribers.removeAll()
        self.sub.removeAll()
    }
    
}
