//
//  Game.swift
//  APIDemo WatchKit Extension
//
//  Created by Sukhwinder Rana on 2019-07-04.
//  Copyright © 2019 Parrot. All rights reserved.
//

import Foundation
import CoreLocation
class Game {
    var gameType: String!
    var city: String!
    var date: String!
    var Location: String!
    var TeamA: String!
    var TeamB: String!
    var Latlng:CLLocationCoordinate2D!
    var Time:String!
    
    
    init(gameType: String, city: String, date: String, location:String, teamA: String, teamB:String, latlng:CLLocationCoordinate2D, time:String) {
        self.gameType = gameType
        self.city = city
        self.date = date
        self.Location = location
        self.TeamA = teamA
        self.TeamB = teamB
        self.Latlng = latlng
        self.Time = time
        
    }
}

