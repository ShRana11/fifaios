//
//  SubscriberCell.swift
//  APIDemo WatchKit Extension
//
//  Created by Sukhwinder Rana on 2019-07-04.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit

class SubscriberCell: NSObject {

    @IBOutlet var dateLabel: WKInterfaceLabel!
    @IBOutlet var imageTeamB: WKInterfaceImage!
    @IBOutlet var imageTeamA: WKInterfaceImage!
}
